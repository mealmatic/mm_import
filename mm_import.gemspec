Gem::Specification.new do |s|
	s.name        = 'mm_import'
	s.version     = '0.0.1'
	s.date        = '2015-01-11'
	s.summary     = "Import recipes"
	s.description = "Import recipes"
	s.authors     = ["Michel Benevento"]
	s.email       = 'michelbenevento@yahoo.com'
	s.files       = ["lib/mm_import.rb", "lib/recipe_importer.rb", "lib/ingredient_importer.rb", "lib/crawler.rb"]
	s.homepage    = 'http://github.com/beno/mm_import'
	s.license     = 'MIT'

	s.add_dependency 'activesupport', '~> 4'
	s.add_dependency 'nokogiri', '~> 1'
	s.add_dependency 'whatlanguage', '~> 1'
	
	s.add_development_dependency 'rake', '~> 10'
	s.add_development_dependency 'minitest', '~> 5.5'
	s.add_development_dependency 'rack-test', '~> 0.6'
	s.add_development_dependency 'activerecord', '~> 4'
	s.add_development_dependency 'pg', '~> 0'
	s.add_development_dependency 'pg_search', '~> 0'

end