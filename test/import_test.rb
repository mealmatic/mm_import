require 'test_helper'

class ImportTest < Minitest::Test
	
	def setup
		@recipe = ::Recipe.find(93369)
		@url = "http://www.okoko.nl/recept/bakrecepten/chocoladetaart/chocoladetaart-extra-puur"
	end
	
	def test_import
		recipe = Recipe.import @url
		assert_equal @recipe.name, recipe.name
		refute_equal 0, recipe.ingredients.count
		ingredient = recipe.ingredients[0]
		product = Product.find(ingredient.product_id)
		assert_equal product.name_nl, ingredient.product_name_nl
		assert_equal product.name_en, ingredient.product_name_en
	end
end
