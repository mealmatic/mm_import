require "minitest/autorun"
require 'mm_import'
require 'active_record'
require 'pg_search'

project_root = File.dirname(File.absolute_path(__FILE__))
Dir.glob File.join(project_root, '../../admin/app/models/*'), &method(:require)

ActiveRecord::Base.establish_connection('postgres://michel:@localhost:5432/mealmatic-development')

I18n.available_locales = [:nl, :en]

class Recipe
	include Import
end