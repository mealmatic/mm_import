require 'active_support'
require 'open-uri'
require 'ingredient_importer'

module Import
	class RecipeImporter
		
		cattr_accessor :importers
		# cattr_accessor :domains
		# cattr_accessor :selectors
		
		# self.selectors = {}
		
		LANGUAGES = {dutch: :nl, english: :en}

		def self.url
			@url
		end
		
		def self.url=(url)
			@url = url
		end

		def self.domains
			@domains
		end
		
		def self.domains=(domains)
			@domains = domains
		end
		
		def self.selectors
			@selectors || {}
		end
		
		def self.selectors=(selectors)
			@selectors = selectors
		end
				
		def self.inherited(child)
			if !	self.importers
				self.importers = []
			end
			self.importers << child
		end
	
		def self.importer_for(url)
			self.importers.each do |importer|
				if importer.can_parse(url)
					return importer.new(url)
				end
			end
			raise "no importer for url: #{url}"
		end
		
		def self.can_parse(url)
			url = URI(url) if url.is_a?(String)
			host = URI(url).host
			pat = /#{self.domains.join('|')}$/
			!host.match(pat).blank?
		end
		
		def load
			@doc = Nokogiri::HTML(open(@url))
		end
		
		def initialize(url)
			@url = URI(url) if url.is_a?(String)
			@attributes = {}
		end
			
		def selector(name)
			self.class.selectors[name]
		end
		
		def select(name)
			@doc.css(selector(name))
		end
		
		def text_at(name, parent = @doc)
			node = parent.at_css(selector(name))
			node ? node.text : ''
		end
	
		def attr_at(name, atr, parent = @doc)
			node = parent.at_css(selector(name))
			node ? node.attributes[atr] : ''
		end

		def parse_name(value = text_at(:name))
			@attributes[:name] = value
		end
		
		def parse_servings(value = text_at(:servings))
			pattern = /([\d]+)[\s]*([\w]*)/
			matches = value.match(pattern)
			return if matches.blank?
			@attributes[:servings] = matches[1]
			unit = Unit.where(type:'serving').simple_search(matches[2], @language)
			if unit
				@attributes[:unit_id] = unit.id
			end
		end
		
		def parse_cooking_time(value = text_at(:cooking_time))
			minutes = 0
			if value.present?
				pattern = /([\d]+):?([\d]*)([MH]*)/
				matches = value.match(pattern)
				if matches
					minutes = matches[1].to_i unless matches[1].blank?
					if matches[3] == 'H' || (matches[3] == '' && minutes < 5)
						minutes = 60*minutes
						minutes += matches[2].to_i unless matches[2].blank?
					end
				end
			end
			@attributes[:time_cooking] = minutes
		end
		
		def parse_tags
			tags = select(:tags).map do |tag|
				if known_tag = Tag.parse(tag.text, @language)
					known_tag.id
				end
			end.compact
			@attributes[:tags] = tags
		end
		
		def parse_image
			node = @doc.at_css(selector(:image))
			img = if node && node.attributes['src']
				node.attributes['src'].value
			end
			if img.present? && !img.match(/^http/)
				img = @url.scheme + '://' + @url.host + img
			end
			@attributes[:source_image_url] = img
		end
		
		def detect_language
			text = @doc.css('p').text
			lang = text.language
			LANGUAGES[lang] && LANGUAGES[lang].to_sym
		end
		
		def new_recipe
			recipe = ::Recipe.new(@attributes)
			ingredients = select(:ingredients)
			ingredients.each do |ingr|
				ingredient = Import::IngredientImporter.new(@language).parse(ingr.text)
				return if @auto && !ingredient.product
				if recipe.servings && recipe.servings > 0
					ingredient.quantity = ingredient.quantity.to_f/recipe.servings
				end
				existing = recipe.ingredients.find do |ingr|
					ingr.product_id == ingredient.product_id && ingr.unit_id == ingredient.unit_id
				end
				if existing
					existing.quantity += ingredient.quantity
				else
					recipe.ingredients << ingredient
				end
			end
			recipe
		end
		
		def import_url
			import(Nokogiri::HTML(open(@url)))
		end
		
		def auto_import(doc, &blk)
			@auto = true
			import(doc, &blk)
		end
		
		def import(doc)
			@doc = doc
			@language = detect_language
			return unless @language
			I18n.locale = @language
			@attributes = {source_url:@url.to_s, source_language: @language}
			parse_name
			parse_servings
			parse_tags
			parse_image
			parse_cooking_time
			recipe = new_recipe
			yield(recipe) if block_given?
			recipe
		end
			
	end
end

