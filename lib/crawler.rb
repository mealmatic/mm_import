require 'ohm'
require 'mm_import'

class SiteCrawler

	SITES = {
		# "www.okoko.nl/recept/" => "/recept/",
		# "www.ah.nl/allerhande/" => "/allerhande/recept/"
		# 'www.food.com/browse/allrecipes/' => "/recipe/[^?\.\/]*$"
		'allrecipes.com/recipes' => "/[Rr]ecipe/.*/[Dd]etail.aspx"

	}
	def self.crawl
		SITES.each_pair do |url, path|
			Anemone.crawl("http://#{url}") do |crawler|
				crawler.on_pages_like(/#{path}/) do |page|
					begin
						parts = page.url.to_s.split('?')
						p parts[0]
						WebPage.create(url: parts[0], body: page.body, imported: 0)
					rescue Ohm::UniqueIndexViolation => e
						parts = page.url.to_s.split('?')
						WebPage.find(url: parts[0]).first.update(imported: 0)
					end
				end
			end
		end
	end

end