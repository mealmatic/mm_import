require 'strscan'

module Import
	class IngredientImporter
		
		def initialize language
			@language = language
			@quantity = 1
			@unit = nil
			@product = nil
			@notes = {}
			@options = []
			@import_line = ''
			@anchor = 0
		end
		
		FRACTIONS  = {"¼" => 0.25, "½" => 0.5, "¾" => 0.75, "⅓" => 1.0/3, "⅔" => 2.0/3, "⅕" => 0.2, "⅖" => 0.4, "⅛" => 0.125}
		STOP_WORDS = {}
		STOP_WORDS[:nl] = %w{ tje jes }
		STOP_WORDS[:en] = %w{ and }
		
		def reset
			
		end
		def parse input
			@notes = {}
			@anchor = 0
			@import_line = input
			@string = StringScanner.new(input)
			scan_quantity
			scan_unit
			scan_product
			::Ingredient.new(attributes)
		end
		
		def scan_fraction(quantity)
			q = if FRACTIONS.keys.include? quantity[-1]
				quantity[0..-2].to_f + FRACTIONS[quantity[-1]]
			else
				quantity.to_f
			end
			if @string.match?(/( \d)?\/\d/)
				fraction = @string.scan(/( \d)?\/\d/)
				matches = fraction.match(/(\d)?\/(\d)/)
				if matches[1].present?
					fraction = matches[1].to_f / matches[2].to_f
					q = q + fraction
				else
					q = q / matches[2].to_f
				end
			end
			q.to_s
		end
		
		def scan_quantity
			if @string.eos?
				@notes[:q] = []
				@string.pos = @anchor
				return
			end
			prefix = @string.scan(/[^\p{N}]*/)
			quantity = @string.scan(/[\p{N}]*/)
			if quantity.present?
				@quantity = scan_fraction(quantity)
				add_note :q, prefix
				@anchor = @string.pos
			else
				scan_quantity
			end
		end
		
		def scan_unit(try=1)
			if @string.eos? || try > 2
				@string.pos = @anchor
				@notes[:u] = []
				return
			end
			@string.skip(/[^\p{L}]*/)
			@string.skip(/[^\)\(]*\)/)
			query = @string.scan /[\p{L}'-]+/
			unit = Unit.where(type:'ingredient').simple_search(query, @language)
			if unit
				@anchor = @string.pos
				@unit = unit
				@string.skip(/[\/-][^ ]*/)
			else
				add_note(:u, query) if query
				scan_unit(try+1)
			end
		end
			
		def scan_product
			if @string.eos?
				@string.pos = @anchor
				return
			end
			@string.skip(/[^\p{L}]*/)
			query = @string.scan /[^,\(]*/
			products = Product.quick_search(query, @language)
			if products.any?
				@anchor = @string.pos
		  		@product = products[0]
		  		product_name = @product.send("name_#{@language}").dup
		  		match = nil
				while !match && product_name.length > 1
					match = query.match(/#{Regexp.escape(product_name)}/i)
					product_name.chop!
				end
				if match
					add_note :p, match.pre_match, true
					add_note :p, match.post_match, true
				end
				add_note :p, @string.rest
		  		if products[1]
		  			@options = products[1..-1]
		  		end
			else
				add_note :p, query
				scan_product
			end
		end

		def add_note type, input, strip = false
			input.gsub!(/^[ ,\(\)-]*|[ ,\)\(-]*$/, '')
			if strip
				alternative_quantifier = I18n.t("search.alternative_separator", locale:@language)
				input.gsub!(/^(?!#{alternative_quantifier})[\w']{1,2}\b ?/, '')
			end
			return if STOP_WORDS[@language].find do |word|
				input.match(/^\w{0,2}#{word}$/)
			end
			return if input.length < 2
			if input.length > 20
				input = input[0..16] + '...'
			end
			@notes[type] ||= []
			@notes[type] << input
		end
		
		def cleanup(input)
			input.gsub(/[\n\r]/, '').strip.gsub(/[ ]{2,}/, ' ')
		end
		
		def attributes
			{quantity: @quantity,
			 unit: @unit,
			 product: @product,
			 product_options: @options,
			 note: @notes.values.flatten.reject{|n| n.blank? }.join(', '),
			 import_line: cleanup(@import_line)}
		end
			
	end
	
end
