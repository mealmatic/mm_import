require 'mm_import'

module Import::Cli
	
	def valid(recipe)
		invalid = recipe.ingredients.find do |ingr|
			ingr.product_id == nil || ingr.quantity == 0
		end
		!invalid
	end
	
	
	def import_all
		ActiveRecord::Base.logger = nil
		WebPage.find(imported: 0).each do |page|
			page.import do |recipe|
				if valid(recipe)
					p page.url
					recipe.save
					if !recipe.errors.any?
						page.update imported: 1
					else
						p recipe.errors
					end
				else
					p '!! ' + page.url
				end
			end
		end
	end
	
end