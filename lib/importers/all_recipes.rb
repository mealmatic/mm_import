module Import
	class AllRecpies < Import::RecipeImporter

		self.url = 'www.allrecipes.com'
		
		self.domains = ['allrecipes.com']
		self.selectors = {
			name: 'h1[itemprop="name"]',
			ingredients: '[itemprop="ingredients"]',
			servings: '[itemprop="recipeYield"]',
			cooking_time: '[itemprop="cookTime"]',
			image: 'img#imgPhoto'
		}
		
		def parse_cooking_time
			time = attr_at(:cooking_time, 'datetime')
			value = time.present? ? time.value : nil
			super(value)
		end
		
	end

end


