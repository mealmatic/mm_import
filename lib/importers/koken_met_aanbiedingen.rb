module Import
	class KokenMetAanbiedingen < Import::RecipeImporter
	
		self.url = 'www.kokenmetaanbiedingen.nl'
	
		self.domains = ['kokenmetaanbiedingen.nl']
		self.selectors = {
			name: '.hrecipe .fn',
			ingredients: '.ingredient',
			servings: '.yield',
			cooking_time: '.cooktime > .value-title',
			tags: '.tag',
			image: 'img.photo',
		}
				
		def parse_cooking_time
			time = attr_at(:cooking_time, 'title')
			value = time.present? ? time.value : nil
			super(value)
		end
		
	end
end

