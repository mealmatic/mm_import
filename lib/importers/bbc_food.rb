module Import
	class BbcFood < Import::RecipeImporter

		self.url = 'www.bbc.co.uk/food'
		
		self.domains = ['bbc.co.uk']
		self.selectors = {
			name: '.article-title > h1',
			ingredients: '.ingredient',
			servings: '.yield',
			cooking_time: '.cookTime > .value-title',
			tags: '.tag',
			image: 'img.photo',
		}
	end

	def parse_cooking_time
		time = @doc.at_css(selector(:cooking_time))
		time = time ? time.attributes['title'].value : nil
		super(time) unless time.blank?
	end

end
