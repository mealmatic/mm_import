module Import
	class Yummly < Import::RecipeImporter

		self.domains = ['yummly.com']
		self.selectors = {
			name: '[itemprop="name"]',
			ingredients: '.ingredient',
			servings: '.yield',
			cooking_time: '[property="yummlyfood:time"]',
			tags: '.tag',
			image: 'img[itemprop="image"]'
		}
	
		def parse_name
			name = @doc.at_css(selector(:name))
			name = name ? name.attributes['title'].value : nil
			super(name) unless name.blank?
		end
		
		def parse_cooking_time(value=nil)
			time = attr_at(:cooking_time, 'content')
			value = time.present? ? time.value.match(/\d*/)[0] : nil
			super(value)
		end
	
	end
end
