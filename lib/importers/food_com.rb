module Import
	class FoodCom < Import::RecipeImporter

		self.url = 'www.food.com'

		self.domains = ['food.com']
		self.selectors = {
			name: 'h1[itemprop="name"]',
			ingredients: '[itemprop=ingredients]',
			servings: '#servingssize',
			cooking_time: '[itemprop="cookTime"]',
			tags: '[itemprop="tag"]',
			image: '[itemprop="image"]',
		}
	
	
		def parse_servings
			text = text_at(:servings)
			match = text.match /(\d+)/
			if match
				super "#{match[1]} servings"
			else
				super
			end
		end
			
		def parse_cooking_time
			time = attr_at(:cooking_time, 'content')
			value = time.present? ? time.value : nil
			super(value)
		end


	end
	
	
end

#
# [].forEach.call(document.querySelectorAll('[itemprop=ingredients]'), function(ingr) {recipe.ingredients.push(parseIngredient(ingr))});
# recipe.name = text('h1[itemprop="name"]');
# var servings = parseServings(text('[itemprop="recipeYield"]'));
# for (var key in servings) {recipe[key] = servings[key]};
# var timeNode = document.querySelector('[itemprop="totalTime"]');
# recipe.timeCooking = parseTime(timeNode ? timeNode.attributes['content'].value : text('.cooking-time'));
# var imageNode = document.querySelector('[itemprop="image"]');
# recipe.illustrationUrl = imageNode ? imageNode.attributes['src'].value : '';
