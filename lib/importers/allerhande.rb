module Import
	class AllerHande < Import::RecipeImporter
	
		self.url = 'www.ah.nl/allerhande/'

		self.domains = ['ah.nl']
		self.selectors = {
			name: 'h1[itemprop="name"]',
			ingredients: '[itemprop="ingredients"]',
			servings: '[itemprop="recipeYield"]',
			cooking_time: '[itemprop="cookTime"]',
			tags: '[itemprop="recipeCategory"]',
			image: 'img.recipe-image'
		}
	
		def parse_cooking_time
			time = @doc.at_css(selector(:cooking_time))
			time = time ? time.attributes['datetime'].value : nil
			super(time) unless time.blank?
		end
	
	
	end
end

=begin

recipe = {ingredients:[], tags:[]};
[].forEach.call(document.querySelectorAll('[itemprop="ingredients"]'), function(ingr) {recipe.ingredients.push(ingr.textContent)});
recipe.name = text('h1[itemprop="name"]');
var servings = parseServings(text('[itemprop="recipeYield"]'));
for (var key in servings) {recipe[key] = servings[key]};
var timeNode = document.querySelector('[itemprop="cookTime"]');
recipe.timeCooking = parseTime(timeNode ? timeNode.attributes['datetime'].value : text('.cooking-time'));
recipe.tags.push({context:'courses', name:text('[itemprop="recipeCategory"]')});
recipe.tags.push({context:'cuisine', name:text('[itemprop="recipeCuisine"]')});
var imageNode = document.querySelector('img.recipe-image');
recipe.illustrationUrl = imageNode ? imageNode.attributes['src'].value : '';
return recipe;

=end