require 'nokogiri'
require 'whatlanguage'
require 'recipe_importer'
require 'ingredient_importer'

project_root = File.dirname(File.absolute_path(__FILE__))
Dir.glob(File.join(project_root, 'importers/*')).each {|f| require f }

module Import
	
	def all_urls
		importers.map do |i|
			i.url
		end
	end
	
end